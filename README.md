# XD75 ortholinear keyboard minimalist case (*WIP*)

## Thingiverse: https://www.thingiverse.com/thing:2854069

- Heavy remix of https://www.thingiverse.com/thing:2739651, a 3 part minimalist case for the satan made by avocado_boat

- Use the STLs from avocado_boat for the 2d body projection of the case

- Make use of my modular hex grid / pattern library available at https://gitlab.com/drz-openscad/modulable-hex-desk-stands

# Screenshots

#### All parts
![all parts](2018-04-08_15-09-31.png)

#### Build view
![build view](2018-04-08_15-07-05.png)

#### Dev view
![dev view](2018-04-08_15-04-15.png)

### Copyright (C) 2018 DrZoid https://gitlab.com/drz-openscad/xd75-minimalist-case